import java.io.*;
import java.util.Scanner;

public class Shortener {
    public static final int LINE_LIMIT = 45;

    public Shortener() {
        Scanner input = new Scanner(System.in);
        System.out.println("Input SRT path: ");
        String filename = input.nextLine();
        input.close();


        try (FileInputStream fileInputStream = new FileInputStream(filename)) {
            StringBuilder result = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));
            String line;

            while ((line = reader.readLine()) != null) {
                result.append(processLine(line) + "\n");
            }

            reader.close();

            PrintWriter printWriter = new PrintWriter(new FileWriter(filename));
            printWriter.println(result.toString());
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String processLine(String input) {
        StringBuilder result = new StringBuilder();

        if (input.length() > LINE_LIMIT) {
            int spaceIndex = LINE_LIMIT;
            for (int i=LINE_LIMIT;i>=0;i--) {
                if (input.charAt(i) == ' ') {
                    spaceIndex = i;
                    break;
                }
            }

            result.append(input.substring(0, spaceIndex));
            result.append("\n");

            result.append(processLine(input.substring(spaceIndex)));
        } else {
            return input;
        }

        return result.toString();
    }
}
